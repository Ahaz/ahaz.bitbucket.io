export const MASK_PRESETS = {
    1: {
        "grayscale" : {"value": 100, "unit": "%"},
        "brightness" : {"value": 113, "unit": "%"},
        "contrast" : {"value": 93, "unit": "%"},
        "invert" : {"value": 2, "unit": "%"},
        "saturate" : {"value": 410, "unit": "%"},
        "sepia" : {"value": 31, "unit": "%"}
    },
    2 : {
        "grayscale" : {"value": 24, "unit": "%"},
        "brightness" : {"value": 101, "unit": "%"},
        "contrast" : {"value": 113, "unit": "%"},
        "opacity" : {"value": 95, "unit": "%"},
        "saturate" : {"value": 222, "unit": "%"}
    },
    3: {
        "grayscale" : {"value": 29, "unit": "%"},
        "brightness" : {"value": 91, "unit": "%"},
        "contrast" : {"value": 103, "unit": "%"},
        "opacity" : {"value": 95, "unit": "%"},
        "saturate" : {"value": 301, "unit": "%"},
        "sepia" : {"value": 0, "unit": "%"},
        "blur" : {"value": 0, "unit": "px"}
    },

    4: {
        "grayscale" : {"value": 52, "unit": "%"},
        "brightness" : {"value": 89, "unit": "%"},
        "contrast" : {"value": 111, "unit": "%"},
        "hue-rotate" : {"value": 0, "unit": "deg"},
        "opacity" : {"value": 93, "unit": "%"},
        "invert" : {"value": 11, "unit": "%"},
        "saturate" : {"value": 282, "unit": "%"},
        "sepia" : {"value": 27, "unit": "%"},
        "blur" : {"value": 1, "unit": "px"}
    },
    5: {
        "grayscale" : {"value": 52, "unit": "%"},
        "brightness" : {"value": 90, "unit": "%"},
        "contrast" : {"value": 111, "unit": "%"},
        "hue-rotate" : {"value": 83, "unit": "deg"},
        "opacity" : {"value": 93, "unit": "%"},
        "invert" : {"value": 5, "unit": "%"},
        "saturate" : {"value": 500, "unit": "%"},
        "sepia" : {"value": 0, "unit": "%"},
        "blur" : {"value": 1, "unit": "px"}
    },
    6: {
        "grayscale" : {"value": 8, "unit": "%"},
        "brightness" : {"value": 99, "unit": "%"},
        "contrast" : {"value": 111, "unit": "%"},
        "hue-rotate" : {"value": 12, "unit": "deg"},
        "opacity" : {"value": 93, "unit": "%"},
        "invert" : {"value": 0, "unit": "%"},
        "saturate" : {"value": 500, "unit": "%"},
        "sepia" : {"value": 100, "unit": "%"},
        "blur" : {"value": 1, "unit": "px"}
    },
    7: {
        "brightness" : {"value": 119, "unit": "%"},
        "contrast" : {"value": 117, "unit": "%"},
        "saturate" : {"value": 110, "unit": "%"},
        "blur" : {"value": 0, "unit": "px"}
    },
    8: {
        "grayscale" : {"value": 100, "unit": "%"},
        "sepia" : {"value": 100, "unit": "%"},
    },
    9: {
        "saturate" : {"value": 454, "unit": "%"},
        "invert" : {"value": 87, "unit": "%"}
    }
};