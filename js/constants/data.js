export const TILE_DATA = [
    {
        id: 1,
        url: "images/uploads/01.jpg",
        title: "MuJuice",
        author: "Jacob Cummings",
    },
    {
        id: 2,
        url: "images/uploads/02.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 3,
        url: "images/uploads/03.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 4,
        url: "images/uploads/04.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 5,
        url: "images/uploads/05.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 6,
        url: "images/uploads/06.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 7,
        url: "images/uploads/07.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 8,
        url: "images/uploads/08.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    },
    {
        id: 9,
        url: "images/uploads/09.jpg",
        title: "MuJuice",
        author: "Donald Trump",
    }
];