export class BaseComponent {
    constructor(selector, data) {
        this.el = $(selector);
        this.el.ready(() => {
            // works for dynamic markup
            // but not static html
            this.loadedCallback();
        });
        this.data = data;
        this.init();
    }

    init() {
        let path = import.meta.url.split('/');
        // remove folder name and component name   
        path = path.slice(0, path.length - 2);
        // figure out component name from constructor     
        let cName = this.toKebab(this.constructor.name);
        let htmlfLoc = `${path.join('/')}/${cName}/${cName}.html`;
        let cssfLoc = `${path.join('/')}/${cName}/${cName}.css`;
        // attach component style, if not loaded already
        if ($(`link[href="${cssfLoc}"]`).length < 1) {
            $.get(cssfLoc) // check css file exists
                .done(() => {
                    $('body').append(`<link rel="stylesheet" type="text/css" href="${cssfLoc}">`);
                })
                .fail(function () {
                    // not exists code
                })
        }
        // attach html
        //attempt static markup
        if (!this.getMarkup) {
            $.get(htmlfLoc)
                .done(() => {
                    // .ready not triggered by .load for some reason
                    this.el.load(htmlfLoc, () => {
                        //loaded hook
                        this.loadedCallback();
                        //this.el.trigger('base-loaded',{id:'value'});
                    });
                })
        } else {
            this.el.append(this.getMarkup());
        }

    }
    loadedCallback() {
        this.attachHandlers();
    }

    attachHandlers() {

    }

    toKebab(str) {
        return str.split('').map((letter, idx) => {
            return letter.toUpperCase() === letter
                ? `${idx !== 0 ? '-' : ''}${letter.toLowerCase()}`
                : letter;
        }).join('');
    }
}