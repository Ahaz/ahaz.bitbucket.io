import { BaseComponent } from "/js/components/base-component/base-component.js";

export class Tile extends BaseComponent {
    constructor(id, tileData) {
        super(id, tileData);
    }

    getMarkup() {
        return `
            <li data-id="${this.data.id}" class="col-md-2 post tile">
                <figure class="tile-container">
                    <img class="tile-image" src="${this.data.url}">
                    <figcaption>
                        <h3>${this.data.title}</h3>
                        <span>${this.data.author}</span>
                    </figcaption>
                </figure>
            </li>
        `;
    }
    loadedCallback() {
        super.loadedCallback();
        console.log('tile loaded');
    }
    attachHandlers() {
        let self = `li.tile[data-id=${this.data.id}]`;
        this.el.find(self).click(this.handleClick.bind(this));
    }
    handleClick(e) {
        this.el.trigger('showEditor', { id: this.data.id, url: this.data.url });
    }
    getImage() {
        let img = `li.tile[data-id=${this.data.id}] .tile-image`;
        return this.el.find(img);
    }
}