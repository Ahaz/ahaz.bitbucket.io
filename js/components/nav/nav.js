import { BaseComponent } from "/js/components/base-component/base-component.js"

export class Nav extends BaseComponent {
    constructor(id) {
        super(id);

    }
    loadedCallback() {
        //super.loadedCallback();   
        console.log('nav loaded');
        $(".icon").click(function () {
            $(".menu").slideToggle(175);
            $(this).toggleClass("opened");

            if ($(window).width() > 992) {
                $('.site-header h1').slideToggle(300);
            }
        });
    }
}