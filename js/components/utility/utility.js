export function createFilterString(filterDef) {
    let filterString = '';
    for (let filterVal in filterDef) {
        filterString += `${filterVal}(${filterDef[filterVal].value}${filterDef[filterVal].unit})`;
    }
    return filterString;
}