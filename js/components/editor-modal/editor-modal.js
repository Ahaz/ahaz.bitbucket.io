import { MASK_PRESETS } from "/js/constants/maskPresets.js";

import { BaseComponent } from "/js/components/base-component/base-component.js"
import { createFilterString } from "/js/components/utility/utility.js"

export class EditorModal extends BaseComponent {
    constructor(id) {
        super(id);
    }

    attachHandlers() {
        this.el.find('.modal__close').on('click', () => {
            this.hide();
        });
        this.el.find(".css-filter-type").on('change', (e) => {
            this.decorateImg(e);
        });
        this.el.find(".slider__reset").on('click', (e) => {
            this.resetFilter(e);
        });
        this.el.find(".modal__filters__item__thumb").on('click', (e) => {
            this.setFilter(e);
        });
        this.el.find(".modal__confirm").on('click', (e) => {
            this.saveState(e);
        });
    }

    loadedCallback() {
        super.loadedCallback();
        this.photo = $('#imageContainer > img');

        $.each(this.el.find('.modal__filters__item__thumb'), (i, filterItem) => {
            let fi = $(filterItem);
            if (fi.css("filter") === "none") {
                fi.css("filter", createFilterString(MASK_PRESETS[i + 1]));
            }
        });

    }

    decorateImg(e) {
        let currentRange = $(e.currentTarget),
            currentFilter = currentRange.data("filter"),
            currentUnit = currentRange.data("unit"),
            filterList = '';

        this.filters[currentFilter] = {
            value: currentRange.val(),
            unit: currentUnit
        }
        $.each(this.filters, (filterName, filterData) => {
            filterList += `${filterName}(${filterData.value}${filterData.unit})`;
        });
        this.photo.css("filter", filterList);
    }

    resetFilter(e) {
        this.el.find('#imageContainer > img').css("filter", "");
        this.filters = {};
    }

    setFilter(e) {
        let target = $(e.currentTarget);
        let filterValue = MASK_PRESETS[target.data("id")];
        this.filters = { ...MASK_PRESETS[target.data("id")] }; // clone to avoid changes trickling to static map
        this.setSliderValues(filterValue);
    }

    setSliderValues(filterValue) {
        // set preview frame style to match filter
        this.photo.css("filter", createFilterString(filterValue));
        // set slider values
        $("#imageEditor").get(0).reset();
        for (let filter in filterValue) {
            $(`input[data-filter="${filter}"]`).val(filterValue[filter].value);
        }
    }

    saveState(e) {
        let photoId = this.photo.data("id");
        this.hide();
        // {new:5, items: [{id: 123, filters:[], data:{id: 1, src:}}]}
        let ls = localStorage.getItem('viewport');
        let gallery = ls ? JSON.parse(ls) : [];
        let state = gallery.filter((item) => { return item.id === this.sId; })[0] || {};

        state.filters = this.filters;
        if (!state.id) {
            state.id = new Date().getTime();
            state.data = this.data;
            state.data.author = 'User';
            state.data.title = state.id;
            gallery.push(state);
        }

        localStorage.setItem('viewport', JSON.stringify(gallery));
        // emit event to re-render grid
        this.el.trigger('re-render-grid');
    }

    show(data, filters, sId) {
        this.el.removeClass('modal-hidden');

        // set preview frame
        this.photo.attr("src", data.url);
        this.photo.data("id", data.id);
        this.data = data;
        this.filters = filters;
        this.sId = sId;
        // set preview filters state and sliders
        filters && this.setSliderValues(filters);

        // set filters thumbs pictures
        this.el.find('.modal__filters__item__thumb').attr("src", data.url);
    }

    hide() {
        this.el.addClass('modal-hidden');
    }
}