import { Tile } from "/js/components/tile/tile.js";
import { createFilterString } from "/js/components/utility/utility.js";

export class StatefulTile extends Tile {

    constructor(id, tileData) {
        super(id, tileData.data);
        this.tileData = tileData;
    }

    loadedCallback() {
        super.loadedCallback();
        // apply filters
        this.applyFilters();
    }
    //override click function from base tile 
    //to pass different object structure
    handleClick(e) {
        this.el.trigger('showEditor', [this.tileData.data, this.tileData.filters, this.tileData.id]);
    }
    applyFilters() {
        super.getImage().css("filter", createFilterString(this.tileData.filters));
    }

}