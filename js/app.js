import { TILE_DATA } from "/js/constants/data.js";


import { EditorModal } from "/js/components/editor-modal/editor-modal.js";
import { Tile } from "/js/components/tile/tile.js";
import { Nav } from "/js/components/nav/nav.js";


class TileView {
    constructor(id, tileDatas) {
        // root element reference
        this.el = $(id);
        this.tiles = [];
        this.editor = new EditorModal('#editor');
        this.nav = new Nav('.site-header');

        // load tiles        
        $.each(tileDatas, (index, tileData) => {
            this.tiles.push(new Tile('.site-main > ul', tileData));
        })

        this.addHandlers();
    }
    addHandlers() {
        this.el.on('showEditor', (e, data) => {
            this.showEditor(e, data);
        })
    }
    showEditor(e, data) {
        console.log('showing editor', data);
        this.editor.show(data);
    }
}

let tv = new TileView("#home", TILE_DATA);
