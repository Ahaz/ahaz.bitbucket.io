import { EditorModal } from "/js/components/editor-modal/editor-modal.js";
import { StatefulTile } from "/js/components/stateful-tile/stateful-tile.js";
import { Tile } from "/js/components/tile/tile.js";
import { Nav } from "/js/components/nav/nav.js";

class LibraryView {
    constructor(id) {
        // root element reference
        this.el = $(id);
        this.editor = new EditorModal('#editor');
        this.nav = new Nav('.site-header');
        this.renderGrid();
        this.addHandlers();
    }
    /* Method to pull gallery state from local storage
    *
    */
    loadState() {
        // {new:5, items: [{id: 123, filters:[], data:{id: 1, src:}}]}
        let ls = localStorage.getItem('viewport');
        let gallery = ls ? JSON.parse(ls) : [];
        return gallery;
    }
    addHandlers() {
        this.el.on('showEditor', (e, data, filters, sId) => {
            this.showEditor(e, data, filters, sId);
        });
        this.el.on('re-render-grid', (e) => {
            this.renderGrid(e);
        });
    }
    showEditor(e, data, filters, sId) {
        this.editor.show(data, filters, sId);
    }
    renderGrid() {
        $('.site-main > ul').empty();
        // load tiles
        this.tiles = [];
        let tileDatas = this.loadState();
        if (tileDatas.length > 0) {
            $.each(tileDatas, (index, tileData) => {
                this.tiles.push(new StatefulTile('.site-main > ul', tileData));
            })
        }
    }
}

let lv = new LibraryView("#home");